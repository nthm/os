#!/bin/bash

source /root/.bash_profile
set -e

cd ~
# For cron
echo "▶️ Timezone"
ln -sf  /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

# If you have BitBucket or GitHub etc then there's probably an API to make this easier
# https://developer.atlassian.com/bitbucket/api/2/reference/resource/users/%7Busername%7D/ssh-keys

echo "▶️ Setting SSH keys (not append)"
# EOT in quotes: https://stackoverflow.com/questions/27920806/
cat << 'EOT' > ~/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAA... ❤️
ssh-rsa AAAAB3NzaC1yc2EAAA... ghamesmorgan
EOT

sed -i 's#\#PasswordAuthentication.*no#PasswordAuthentication yes#' /etc/ssh/sshd_config
echo "▶️ SSH systemctl enable --now"
systemctl enable --now sshd

echo -n "▶️ Docker?"
if ! type "docker" > /dev/null; then
  echo " ❌"
  echo "▶️ ▶️ Not found. Downloading Docker"
  BASE="https://download.docker.com/linux/static/stable/x86_64"
  URL="$BASE/$(curl -L $BASE \
  | grep -Po 'docker-\d.*?tgz' \
  | sort \
  | tail -n1)"
  echo "▶️ ▶️ Using $URL"
  curl -O $URL
  # Strip components removes the subdirectory 'docker/'
  tar --directory /usr/local/bin/ --strip-components 1 -zxvf docker*.tgz
  rm docker*.tgz
  chown -R root:root /usr/local/bin/*
  chmod +x /usr/local/bin/docker
  echo "▶️ ▶️ Installed"
else
  echo " 🟢"
fi

echo -n "▶️ Traefik?"
if ! type "traefik" > /dev/null; then
  echo " ❌"
  echo "▶️ ▶️ Not found. Downloading Traefik"
  URL=$(curl -L https://api.github.com/repos/containous/traefik/releases \
  | grep 'browser_download_url.*v2.1.*linux_amd64.tar.gz' \
  | head -n1 \
  | cut -d : -f 2,3 \
  | tr -d \")
  echo "▶️ ▶️ Using $URL"
  curl -L -O $URL
  tar --directory /usr/local/bin/ -zxvf traefik_*_linux_amd64.tar.gz
  rm traefik_*_linux_amd64.tar.gz
  rm /usr/local/bin/*.md
  chown -R root:root /usr/local/bin/*
  chmod +x /usr/local/bin/traefik
  # Allow it to bind to priv ports (80/443) as a non-root user
  setcap 'cap_net_bind_service=+ep' /usr/local/bin/traefik
  echo "▶️ ▶️ Installed"
else
  echo " 🟢"
fi

echo "▶️ Service files Docker/Traefik systemd"
cat << 'EOT' > /etc/systemd/system/traefik.service
[Unit]
Description=Traefik
After=network-online.target
Wants=network-online.target

[Service]
Restart=on-abnormal
ExecStart=/usr/local/bin/traefik --log.level=INFO \
                                 --api=true \
                                 --api.dashboard=true \
                                 # Not easy to setup under /dashboard/ prefix otherwise
                                 --api.insecure=true \
                                 --providers.docker=true \
                                 --providers.docker.exposedbydefault=false \
                                 --entrypoints.http.address=:80

[Install]
WantedBy=multi-user.target
EOT

# From https://github.com/moby/moby/tree/master/contrib/init/systemd
cat << 'EOF' > /etc/systemd/system/docker.service
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target docker.socket firewalld.service
Wants=network-online.target

[Service]
Type=notify
ExecStart=/usr/local/bin/dockerd -H unix:///var/run/docker.sock
ExecReload=/bin/kill -s HUP $MAINPID
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s
EOF

# Try to show what you might need if docker is installed via Yum
mkdir -p /etc/systemd/system/docker.service.d/
cat << 'EOF' > /etc/systemd/system/docker.service.d/override.conf
[Service]
ExecStart=
ExecStart=/usr/local/bin/dockerd -H unix:///var/run/docker.sock -H tcp://0.0.0.0:2375
EOF

echo "Starting Traefik and Docker"
echo "▶️ Traefik and Docker systemctl enable --now"
systemctl daemon-reload
systemctl enable --now traefik docker

# Cron nightly
echo "▶️ Writing ~/service-redeploy.sh"
cat << 'EOT' > ~/service-redeploy.sh
#!/bin/bash
source /root/.bash_profile
set -x

RUNNING=$(docker ps --quiet --filter "label=traefik.enable")
if ! test -z "$RUNNING"; then
  echo "Stopping and Traefik containers: $(echo -- $RUNNING)"
  docker stop $RUNNING
  docker rm $RUNNING
fi

# Without this service will eventually use all available ports and error out
docker network prune --force

function create_service_container () {
  service create \
    --labels \
      traefik.enable=true \
      traefik.http.routers.http.entryPoints=http \
      traefik.http.routers.http.rule=Host\(\`your-lovely-site.com\`\) \
      traefik.http.routers.http.service=http \
      traefik.http.services.http.loadbalancer.server.port=8000
    2>&1 | tee ~/output-service-redeploy.log
}

create_service_container

# Check if it failed to deploy due to a docker pull
# Unfortunately doesn't send a failure exit code when it fails... Great

echo "Testing if service deployment failed by needing an image pull"
PULL_IMAGE=$(grep -oP "ERROR.*Run 'docker pull \K.+(?=')" ~/output-service-redeploy.log | head -n1)
if ! test -z "$PULL_IMAGE"; then
  docker pull $PULL_IMAGE
  create_service_container
fi
EOT

chmod +x ~/service-redeploy.sh

# Everyday at 6am
CRON="0 6 * * * bash -c /root/service-redeploy.sh"

echo "▶️ Overwriting crontab:"
echo "$CRON" | crontab -
crontab -l

echo "▶️ Updating MOTD displayed on SSH login"
cat <<EOT > /etc/motd
Last provisioned on $(date)
Service redeployment schedule "$CRON"
EOT

echo "▶️ OK ☀️"
