```
> code --list-extensions

adam-watters.vscode-color-pick
dbaeumer.vscode-eslint
eamodio.gitlens
firefox-devtools.vscode-firefox-debug
Gruntfuggly.todo-tree
humao.rest-client
jpoissonnier.vscode-styled-components
liviuschera.noctis
mhutchie.git-graph
mkxml.vscode-filesize
ms-vscode-remote.remote-containers
ms-vscode-remote.remote-ssh
ms-vscode-remote.remote-ssh-edit
ms-vscode.cpptools
ms-vscode.wordcount
naumovs.color-highlight
richie5um2.vscode-sort-json
ritwickdey.LiveServer
ryu1kn.partial-diff
stkb.rewrap
tomoki1207.selectline-statusbar
tomsaunders.vscode-workspace-explorer
Yukai.map-replace-js
yzhang.markdown-all-in-one
```
