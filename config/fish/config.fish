#!/usr/bin/fish

# Disable welcome message
set fish_greeting

# Paths in Mac are not /usr/bin so just inherit...
# Replace the broken utilities with GNU/Linux
# set -x PATH /usr/local/opt/util-linux/bin /usr/local/opt/util-linux/sbin $PATH
# set -x PATH /usr/local/opt/coreutils/libexec/gnubin $PATH

# Mac: Manual pages are added at runtime in zsh via /usr/libexec/path_config
# So add them here instead. They'll be processed FIFO
# set -x MANPATH ''
# set -x MANPATH /usr/local/opt/coreutils/libexec/gnuman $MANPATH
# set -x MANPATH /usr/local/opt/util-linux/share/man $MANPATH
# set -x MANPATH /usr/share/man $MANPATH
# set -x MANPATH /usr/share/local/man $MANPATH

# Mac: Python's packages (via pip) aren't in the path by default
# set -x PATH /Users/ghamesmorgan/Library/Python/3.7/bin $PATH

set -x PATH /usr/bin

# Using `n` instead, but:
set NPM_PACKAGES "$HOME/_/npm"
set PATH $NPM_PACKAGES/bin $PATH

# TODO This breaks all man pages? Thankfully npm packages almost never have them
# set MANPATH $NPM_PACKAGES/share/man $MANPATH

set -x N_PREFIX "$HOME/_/n"
set -x PATH $N_PREFIX/bin $PATH

# Personal scripts
set -x PATH $HOME/_/bin $PATH

# For V8 and Chromium{,OS}
set PATH $PATH $HOME/_/depot_tools

if status --is-login
    if test (tty) = '/dev/tty1'
        exec sway >> ~/_/sway.log 2>&1
    end
end

set -x VISUAL nano
set -x EDITOR nano

# Mac: (Maybe one day I'll have a computer fast enough to do this)
# set -x VISUAL code --wait
# set -x EDITOR code --wait

alias systemctl "sudo systemctl"
alias mount "sudo mount"

alias fuzzy "fd --type f | fzf --height 50% --preview='highlight -t 2 -O truecolor {} | head -$LINES'"

function _runsudo --description 'Add sudo'
    set -l cursor_pos (echo (commandline -C) + 5 | bc)
    commandline -C 0
    commandline -i 'sudo '
    commandline -C "$cursor_pos"
end
# "Ctrl-Root"? Ctrl-S is taken because Fish uses it for searching in menus
bind \cr "_runsudo"

function !! --description "Retry with sudo"
    set -l prev_command $history[1]
    if test $status -eq 0
        echo "Exited 0. Not retrying: $prev_command"
        return 1
    end
    echo "sudo $prev_command"
    sudo (string split ' ' $prev_command)
end

functions --query realcd; or functions --copy cd realcd
function cd
    if test "$argv" = "-"
        echo "Hop: $PWD"
    end
    # Using `builtin` would skip Fish's cd function that supports "-"
    realcd $argv
    test $status -eq 0; and ll
end

function ls
    command ls -XFh --group-directories-first --human-readable --sort=extension --color=always $argv
end

# This is only for my laptop where I'm tired of CLI editors in my 2020
# I'd use vscode but it takes 10-20 seconds to launch...
function nano
    swaymsg layout tabbed
    mousepad $argv
    swaymsg layout default
end

# TODO: Replace with real ttyrec
# It's clean and outputs a single file. Even Joey.H uses it instead
# https://git.joeyh.name/index.cgi/zzattic/ttyrec.git/

function ttyrec --description "Record a terminal session into this directory"
    set session_name session.$USER.(date +%d-%m-%Y-%T)
    set session_files $session_name.log $session_name.timing
    touch $session_files
    # Reading a session log from within the session causes an infinite loop
    chmod 200 $session_files
    echo "ttyrec: Recording into $session_name.{log,timing}"
    script -q -t $session_name.log 2> $session_name.timing
    # It's now safe to unlock and read the files
    chmod 660 $session_files
    echo "ttyrec: Done"
end

function ttyplay --description "Play a recorded terminal session" --argument-names "session" "speed"
    if test -z "$session"
        echo "ttyplay: No session provided. Usually '~/session.$USER."(date +%d-%m-%Y-%T)
        return 1
    end
    test -z "$speed"; and set speed 1
    test -f $session.log -a -f $session.timing || exit "Session '$session' doesn't resolve to a '.log' and '.timing' file"
    echo "ttyplay: Playing '$session' at x$speed speed"
    # Start date/time, $TERM, TTY device (/dev/pts/0), and terminal dimentsions
    head -n1 $session.log
    scriptreplay -d $speed --timing $session.timing --typescript $session.log
    # End date/time and command exit status
    tail -n1 $session.log
    echo "ttyplay: Done"
end

fish_ssh_agent
