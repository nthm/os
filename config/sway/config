# Read `man 5 sway` for a complete reference.
set $mod Mod4

set $swaylock swaylock \
--screenshots \
--clock \
--indicator \
--indicator-radius 100 \
--indicator-thickness 7 \
--effect-blur 2x2 \
--ring-color bb00cc \
--key-hl-color 880033 \
--line-color 00000000 \
--inside-color 00000088 \
--separator-color 00000000 \
--text-clear-color 262a3300 \
--text-ver-color 262a3300 \
--text-wrong-color 262a3300 \
--timestr %l:%M

set $mako mako \
--default-timeout 5000 \
--font "Noto Sans 10" \
--background-color "#000000AA" \
--border-color "#FFFFFFAA"

font pango:Noto Serif 8
default_border pixel 2
smart_gaps on
gaps inner 4
hide_edge_borders smart

# Notifications
bindsym $mod+Escape exec makoctl dismiss

# Lock
bindsym $mod+l exec $swaylock

# Brightness
bindsym XF86MonBrightnessUp         exec light -A 10 && light -G | cut -d'.' -f1 > /tmp/wobpipe
bindsym XF86MonBrightnessDown       exec light -U 10 && light -G | cut -d'.' -f1 > /tmp/wobpipe

# Audio
bindsym XF86AudioRaiseVolume        exec --no-startup-id pactl set-sink-volume 0 +5%  && pamixer --get-volume > /tmp/wobpipe
bindsym XF86AudioLowerVolume        exec --no-startup-id pactl set-sink-volume 0 -5%  && pamixer --get-volume > /tmp/wobpipe
bindsym XF86AudioMute               exec --no-startup-id pactl set-sink-mute 0 toggle && pamixer --get-volume > /tmp/wobpipe

# Screenshots
exec mkdir -p "/home/today/Nextcloud/Snips"
set $note notify-send "Screenshot"
set $pics /home/today/Nextcloud/Snips
bindsym --release Print             exec IMG="$pics/`date +%Y-%m-%d-%T`.png" && grim "$IMG" && $note "Screen saved to $IMG"
bindsym --release Shift+Print       exec IMG="$pics/`date +%Y-%m-%d-%T`.png" && REG=$(slurp) && grim -g "$REG" "$IMG" && $note "Region $REG saved to $IMG"
bindsym --release Ctrl+Shift+Print  exec IMG="$pics/_CLIPBOARD.png" && REG=$(slurp) && grim -g "$REG" "$IMG" && wl-copy < $IMG && $note "Region $REG to clipboard"

### Output configuration
output * bg `/home/today/_/sway-shell/wallpaper-path.sh` fill
# output LVDS-1 scale 0.90

# output HDMI-A-1 resolution 1920x1080 position 1920,0
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
exec swayidle -w \
    timeout 300  "$swaylock" \
    timeout 600  'swaymsg "output * dpms off"' \
         resume  'swaymsg "output * dpms on"' \
   before-sleep  "$swaylock"

# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration

### Key bindings

#
# Basics:
#
bindsym $mod+t exec lxterminal
bindsym $mod+q kill

# Note: it's recommended that you pass the final command to sway such as
bindsym $mod+Return exec albert show

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal

bindsym $mod+Shift+c reload
bindsym $mod+Shift+e exec swaynag -t warning -m 'Exit? This will end the session.' -b 'Yes' 'swaymsg exit'

#
# Moving around:
#
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

#
# Workspaces:
#
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5

bindsym $mod+Shift+1 move container to workspace 1; workspace 1
bindsym $mod+Shift+2 move container to workspace 2; workspace 2
bindsym $mod+Shift+3 move container to workspace 3; workspace 3
bindsym $mod+Shift+4 move container to workspace 4; workspace 4
bindsym $mod+Shift+5 move container to workspace 5; workspace 5

bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

#
# Layout stuff:
#
bindsym $mod+h splith
bindsym $mod+v splitv

# Switch the current container between different layout styles
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+Shift+space floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+space focus mode_toggle

# move focus to the parent container
bindsym $mod+a focus parent

#
# Scratchpad:
#
# Sway has a "scratchpad", which is a bag of holding for windows.
# You can send windows there and get them back later.

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

#
# Resizing containers:
#
mode "resize" {
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

exec nm-applet --indicator
exec telegram-desktop
exec nextcloud
exec albert | grep -v DEBG:
exec waybar
exec python3 /home/today/_/sway-shell/fade.py || notify-send "Fade errored"
exec mkfifo /tmp/wobpipe && tail -f /tmp/wobpipe | wob

# Lock the screen immediately since autologin is enabled
exec $swaylock
exec $mako

for_window [window_role="pop-up"] floating enable
