Similar to _server-work-gitlab-runner_, this server is a dedicated VM for
running CICD work. This happens in Jenkins, via a master node on another
machine. I didn't have Jenkins admin access to register the VM as a node, so
work was sent to the VM via SSH and the DOCKER_HOST environment variable.

It works pretty well, but it's a hack.
