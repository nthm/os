# Lychee install (Parabola/Arch)

This is two systems. It was a desktop that replaced the NixOS server, and then
was migrated to a laptop (to save space, power, and use an SSD).

The desktop had full disk encryption using a USB key to skip password entry on
boot; later an initramfs allowing for remote decryption via SSH; systemd-boot
using UEFI; redundancy between two drives using btrfs' mirroring (a bit
complicated because it's a 1TB and 2TB). It was running Parabola.

The laptop runs Arch with Parabola's Linux-Libre kernel. (I don't remember why
that was needed... It may have been that I wanted mainline repositories or
because my laptop will need Arch to support iwlwifi and its less to maintain).
There's only BIOS on the laptop so I needed to use syslinux; I tried barebox to
no avail. The partition scheme was simplified since there's no mirroring.

Migration came at a great time because I was seeing one drive start to fail:

```
[...] ata2.00: exception Emask 0x0 SAct 0x7ffffe03 SErr 0x0 action 0x0
[...] ata2.00: irq_stat 0x40000008
[...] ata2.00: failed command: READ FPDMA QUEUED
[...] ata2.00: cmd 60/08:a8:10:fd:e8/00:00:01:00:00/40 tag 21 ncq dma 4096 in
                         res 41/40:08:10:fd:e8/00:00:01:00:00/00 Emask 0x409 (media error) <F>
[...] ata2.00: status: { DRDY ERR }
[...] ata2.00: error: { UNC }
[...] ata2.00: configured for UDMA/133
[...] sd 1:0:0:0: [sdb] tag#21 FAILED Result: hostbyte=DID_OK driverbyte=DRIVER_SENSE cmd_age=14s
[...] sd 1:0:0:0: [sdb] tag#21 Sense Key : Medium Error [current]
[...] sd 1:0:0:0: [sdb] tag#21 Add. Sense: Unrecovered read error - auto reallocate failed
[...] sd 1:0:0:0: [sdb] tag#21 CDB: Read(10) 28 00 01 e8 fd 10 00 00 08 00
[...] blk_update_request: I/O error, dev sdb, sector 32046352 op 0x0:(READ) flags 0x100 phys_seg 1 prio class 0
[...] BTRFS error (device dm-2): bdev /dev/mapper/spaceB errs: wr 0, rd 8, flush 0, corrupt 0, gen 0
[...] ata2: EH complete
[...] BTRFS info (device dm-2): read error corrected: ino 42756 off 5550080 (dev /dev/mapper/spaceB sector 32040208)
```

Repeatedly. Ouch >.>

Thanks Btrfs

---

From the host:

```
sudo su

# Create partitions for /boot (sde1; 200M) and / (sde2; rest)
gdisk /dev/sde

# LUKS v2 and map to /dev/mapper/lychee
cryptsetup --verify-passphrase --verbose luksFormat /dev/sde2
# Write to the LUKS header that this is an SSD
cryptsetup --allow-discards --persistent open /dev/sde2 lychee
cryptsetup luksDump /dev/sde2 | grep Flags

mkfs.btrfs -L lychee-boot /dev/sde1
mkfs.btrfs -L lychee /dev/mapper/lychee

mount /dev/mapper/lychee /mnt/

# Skip linux and linux-firmware since we'll need the AUR/pakku for linux-libre
# If coming from Parabola use -G and -C with another config
pacstrap /mnt base

# Now that the base filesystem directories exist mount boot
mount /dev/sde1 /mnt/boot/

# Mirrorlist if needed: https://www.archlinux.org/mirrorlist/
curl "https://www.archlinux.org/mirrorlist/?..." -O /mnt/etc/pacman.d/mirrorlist
# Edit config as needed;
nano /mnt/etc/pacman.conf

arch-chroot /mnt
```

From the chroot:

```
pacman -Sy archlinux-keyring
pacman-key --populate archlinux
pacman-key --refresh-keys # Needs a few minutes

# Make sure it works
pacman -Syy
# Pre-build pakku without needing a Nim dependency (currently broken in AUR)
pacman -U /root/pakku-0.14-1-x86_64.pkg.tar.xz
# Patched custom syslinux
pacman -U /root/syslinux-6.04-bios-lychee-x86_64.pkg.tar.xz

pacman -S base

# filesystem  gcc-libs  glibc  bash  coreutils  file  findutils  gawk  grep  procps-ng  sed  tar
# gettext  pciutils  psmisc  shadow  util-linux  bzip2  gzip  xz  licenses  pacman  systemd
# systemd-sysvcompat  iputils  iproute2

pacman -S base-devel

# pacman  systemd  systemd-libudev  systemd-udev  autoconf  automake  binutils  bison  fakeroot
# file  findutils  flex  gawk  gcc  gettext  grep  groff  gzip  libtool  m4  make  patch  pkgconf
# sed  sudo  texinfo  which

pacman -S --needed \
abduco  bat  bind-tools  btrfs-progs  cmake  dhcpcd  docker  docker-compose  dracut  exa  ffmpeg   \
fish  fzf  git  goaccess  gptfdisk  htop  iftop  iotop  less  lsof  man-db  nano  nftables  nnn    \
pigz  pkgfile  postgresql  restic  ripgrep  rmlint  rsync  sshfs  sshguard  strace  tcpdump  time  \
traceroute  tree  wget  wireguard-tools

# AUR
pakku -S \
btrbk  code-server  git-annex-standalone  gotify-cli  ttyrec

echo "LANG=en_CA.UTF-8" > /etc/locale.conf
echo "en_CA.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
echo "lychee" > /etc/hostname
ln -sf /usr/share/zoneinfo/Canada/Pacific /etc/localtime
chsh -s /usr/bin/fish root
timedatectl set-ntp true
systemctl enable --now dhcpcd@enp4s0 systemd-timesyncd
```

Then the bootloader. I like systemd-boot, which is pre installed, but it won't
work on the laptop which isn't UEFI. _Skip to "Generic"_. Below is the desktop
configuration:

Also see [NixOS EFI] in TW for the previous EFI layout that had to be replaced.

```
# Read initramfs.md
pakku -S dracut
dracut -f -v
# There's a pacman hook later on to do this automatically
bootctl install
```

Then add a configuration for /boot/loader/entries/ and point to it in
/boot/loader/loader.conf. I turn off audit because otherwise journal logs are
entirely useless because it's flooded with successful audit logs. Apparently
this is yet another bug/regression in systemd.

> /boot/loader/entries/luks-dracut.conf
```
title     Linux via Dracut
linux     /vmlinuz-linux-libre
initrd    /initramfs-$kernal_version.img
options   rd.luks.uuid=[...] rd.luks.name=[...]=os rd.luks.options=allow-discards audit=0 root=/dev/mapper/os rw
```

> /boot/loader/loader.conf
```
timeout 3
default luks-dracut.conf
console-mode max
editor no
```

Generic:

```
mkdir /etc/pacman.d/hooks/
nano /etc/pacman.d/hooks/systemd-boot.hook # Below

# For btrbk to snapshot home directories and /srv
rm -r /home /srv
btrfs sub create /home
btrfs sub create /srv
btrfs sub create /snapshots # See btrbk.conf

useradd yesterday --create-home --shell=/usr/bin/fish
# Enter
cd /home/yesterday
mkdir -p _ _/bin _/n _/work _/work-theirs _/sessions
mkdir Desktop Downloads
# Setup 'n' for Node
curl -L https://raw.githubusercontent.com/tj/n/master/bin/n -o _/bin/n
chmod +x _/bin/n
# Exit
cd -
```

Disk related

```
# Desktop-only
# sudo hdparm -S 120 /dev/disk/by-id/... # As needed, also see `-B`
```

I'd recommend turning off some services

```
systemctl list-unit-files --state=masked
UNIT FILE                     STATE
auditd.service                masked
avahi-daemon.service          masked
systemd-networkd.service      masked
avahi-daemon.socket           masked
systemd-journald-audit.socket masked
systemd-networkd.socket       masked

12 unit files listed.
```

Here's the hook for systemd-boot:

> /etc/pacman.d/hooks/systemd-boot.hook
```
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
```

Setup the crypttab for systemd to pickup on boot. Note these UUIDs are of the
disk partition not the filesystem and not any entries in /dev/mapper

> /etc/crypttab
```
spaceA   UUID=<...>   /dev/disk/by-id/usb-<...>-0:0-part1   luks,keyfile-size=512
spaceB   UUID=<...>   /dev/disk/by-id/usb-<...>-0:0-part1   luks,keyfile-size=512
```

> /etc/fstab
```
# /dev/mapper/os LABEL=Parabola
UUID=d1ec9fb...   /           btrfs     rw,relatime,space_cache,subvol=@   0 0
# EFI /dev/sda3. After OS /sda1 and Space{A,B} /sd{a,b}2
UUID=FD39-D0...   /boot       vfat      rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro   0 2
# /dev/mapper/Space{A,B}
UUID=13ea3fb...   /space      btrfs     rw,relatime
```

TTY-related changes (which carry over to dracut when `hostonly="yes"`)

> /etc/systemd/system/getty@tty1.service.d/noclear.conf
```
[Service]
TTYVTDisallocate=no
```

TTY12 is special because its always to the left of TTY1

> /etc/systemd/journald.conf.d/forward-to-tty12.conf
```
[Journal]
ForwardToConsole=yes
TTYPath=/dev/tty12
MaxLevelConsole=info
```

> /etc/systemd/system/docker.service.d/override.conf
```
[Service]
ExecStart=
ExecStart=dockerd -H fd:// --userland-proxy=false
```

SSHD banner

```
.-.           .-.
: : .-..-..--.: `-. .--. .--.
: :_: :; '  ..: .. ' '_.' '_.'
`.__`._. ;.__.:_;:_`.__.`.__.
     :__.'
```

## Resources:

- Wiki: https://wiki.archlinux.org/index.php/Dm-crypt/System_configuration
- Another similar setup: https://austinmorlan.com/posts/arch_linux_install
