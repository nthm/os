Network namespacing in systemd

Based off this blog: https://www.mth.st/blog/nixos-wireguard-netns/ but not Nix
because I just couldn't vibe that OS

### Service template for creating a namespace

_/etc/systemd/system/netns@.service_:
```
[Unit]
Description=Network namespace for %I
Before=network.target

[Service]
Type=oneshot
RemainAfterExit=true
PrivateNetwork=true
ExecStart=/etc/systemd/system/netns/ns-up %I
ExecStop=ip netns del %I

[Install]
WantedBy=multi-user.target
```

_/etc/systemd/system/netns/ns-up_
```sh
#!/bin/bash
ip netns add $1
umount /var/run/netns/$1
mount --bind /proc/self/ns/net /var/run/netns/$1
```

### Starting a WireGuard tunnel using that namespace

_/etc/systemd/system/netns-wg.service_
```
[Unit]
Description=WireGuard network interface
BindsTo=netns@wg.service
Requires=network-online.target
After=netns@wg.service

[Service]
Type=oneshot
RemainAfterExit=true
ExecStart=/etc/systemd/system/netns/wg-up
ExecStop=/etc/systemd/system/netns/wg-down
```

_/etc/systemd/system/netns/wg-up_
```sh
#!/bin/bash
PRIVATE_KEY=
PEER=
ENDPOINT=
IPV4_ADDR=
IPV6_ADDR=

ip link add wg type wireguard
wg set wg \
  private-key $PRIVATE_KEY \
  peer $PEER \
  allowed-ips 0.0.0.0/0,::/0 \
  endpoint $ENDPOINT
ip link set wg netns wg up
ip -n wg address add $IPV4_ADDR dev wg
ip -n wg -6 address add $IPV6_ADDR dev wg
ip -n wg route add default dev wg
ip -n wg -6 route add default dev wg
```

_/etc/systemd/system/netns/wg-down_
```sh
#!/bin/bash
ip -n wg link del wg
ip -n wg route del default dev wg
```

### Joining a service to use the namespace

_/etc/systemd/system/yourservice.service_
```
[Unit]
BindsTo=netns-wg.service
After=netns-wg.service
JoinsNamespaceOf=netns@wg.service

[Service]
PrivateNetwork=true
```
