The btrbtk readme on their repository has everything and is well written with
many examples.

```
snapshot_preserve_min   2d
snapshot_preserve       7d 4w 6m

volume /
  snapshot_dir snapshots/
  subvolume .
    snapshot_name @
  subvolume home
  subvolume trees

volume /space
  snapshot_dir Snapshots/
  subvolume Annex
  subvolume Annex-Music
  subvolume Annex-Photos
  subvolume Annex-Private
  subvolume Git
  subvolume Torrents
  subvolume Nextcloud
  subvolume Music
```

```
yesterday@lychee /space> /snapshots/
total 0
drwxr-xr-x 1 root      root       34 May 12 20:46 '@'/
drwxr-xr-x 1 root      root      160 May 12 20:40 '@.20190512'/
drwxr-xr-x 1 root      root       18 May 12 21:14  home.20190512/
drwxr-xr-x 1 yesterday yesterday   8 May 12 20:40  trees.20190512/

yesterday@lychee /space> ll Snapshots/
total 0
drwxr-xr-x 1 yesterday yesterday  138 Apr 29 15:28 'Annex (Lychee)'/
drwxr-xr-x 1 yesterday yesterday  112 Apr 29 15:25 'Annex (Pre-init)'/
drwxr-xr-x 1 yesterday yesterday  206 Apr 29 14:54  Torrents/
drwxr-xr-x 1 yesterday yesterday  138 Apr 29 15:28  Annex.20190512/
drwxr-xr-x 1 yesterday yesterday 1.5K May 12 21:24  Annex-Music.20190512/
drwxr-xr-x 1 yesterday yesterday  104 May 12 21:24  Annex-Photos.20190512/
drwxr-xr-x 1 yesterday yesterday  132 May 12 21:23  Annex-Private.20190512/
drwxr-xr-x 1 yesterday yesterday    0 May 12 21:21  Git.20190512/
drwxr-xr-x 1 yesterday yesterday  206 Apr 29 14:54  Torrents.20190512/
```

Remember to `systemctl enable btrbk.timer`
