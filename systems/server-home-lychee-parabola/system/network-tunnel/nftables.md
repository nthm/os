## Tunnel

WireGuard setup. Mention the issue of AllowedIPs and using `ping -I` to discover
that. I need that on the clients not the server.

## Client

Apply `*.nft` rules using: `sudo nft -ef network/local.nft`

Which is a little restrictive, since it means if I listen on all interfaces for
a webserver people still can't talk to me on my LAN.

Sidenote on oif/iif: https://unix.stackexchange.com/questions/283275#374070

## Server

That last sentence is really important: I want to preserve the source IP of the
client. This introduces an issue since with NAT alone the source IP must be
changed if the home server is to send the response back to the proxy. The above
config will tell the homeserver to reply _directly_ to the client, which they
aren't expecting and won't make sense since they can't reach my internal IP.

The answer is to leverage the WireGuard tunnel and setup a _"Routing Policy"_
that sends all _replies_ that come from the tunnel, back into the tunnel. This
is outside of NAT's scope and of NFTables, so it happens in `iproute2`.

This link was very helpful: https://superuser.com/questions/1357440/. Another
resource, [here](https://superuser.com/questions/690427) explains why you don't
want to masquerade or SNAT in postrouting.

The resources I listed in the file on SSH tunnels didn't care about preserving
remote IPs, and just overwrote the source IP.

Here's an introduction to policy routing:

https://blog.scottlowe.org/2013/05/29/a-quick-introduction-to-linux-policy-routing/

I'll post routing table configurations soon when they're set up.
