# VPS NAT proxy using NFTables

This is the setup for proxying a server at home behind a VPS to get a static
public IP for a domain.

I did this once through SSH tunneling (also in this repo) but needed a way to
maintain the client IP when the connection is proxied This is done with a tunnel
setup as a dedicated interface and then using `ip` to set routes and rules for
traffic to be routed back to the VPS despite the source IP.

In this post I'm using WireGuard as the tunnel, but you can use anything you
want, including some GRE-setups done entirely with the `ip` command. I just
needed the tunnel encrypted.

...

You should look up the NFTables flow chart to see how the hooks are executed and
where priorities come into play, like -150 for MANGLE
