# Linux Libre 5.6.3+

I'm compiling a kernel again. Haven't done that since I used Gentoo. I want to
have a deblobbed kernel, and have used Linux Libre for a year on the Parabola
server which has been great. I'm transitioning hardware (to an unused laptop)
for a new server, and will stick with Arch. I'm basically provisioning 3 systems
right now and one, a Thinkpad X200, won't allow anything other than the
proprietary Intel WiFi card (BIOS will block it and I don't have the tools or
confidence to flash Libreboot) so Parabola won't work. That being said, I still
want as close to a Linux-Libre system.

There's an AUR package for linux-libre, but it takes 5 hours to compile with
their default config. I know when I was on Gentoo which the exact same hardware
it wasn't even 10 minutes. For this laptop-server, I know Radeon will try to
run, and it would be best to not even have it compiled rather than deblobbed.
Kernels are personal enough that I'd like to be comfortable building them. The
Arch makepkg build system is flexible enough to have local git checkouts of
PKGBUILDs to build your own custom version of a package, but I'd rather have
this process be OS independent. I won't be on Arch longterm - I've seen enough
distros like Void, Alpine, and Distri to know I'll be building my own. In the
meantime, I've been thinking about a Exherbo developer I saw who patched/git her
own kernel source each release, so I'm getting into that.

## Sources

If you go look for the source of Gentoo, Arch, and Linux-Libre kernels, it's
very neat to see maybe 10 small patch files. It's not far from upstream, and
there's nothing special from vanilla Linux.

Linux-Libre has a script called _deblob_ which is very long and maintains a
reference to all the blobs found in the kernel, for every version. I would be
able to run the Bash+Python scripts locally on a vanilla checkout, but it's
easier to use the official source:

https://linux-libre.fsfla.org/pub/linux-libre/releases/

They also have all the patches between minor and patch versions. There's no repo
or git, but HTTP directory listings are okay.

I started with _LATEST-5.6.N/linux-libre-5.6.3-gnu.tar.xz_

The only proprietary part of the entire config that I've selected is the Realtek
R8169 ethernet driver. I need that lol. It works without the proprietary
firmware! There's a dmesg error logged for "/* DEBLOBBED */".

Here's the firware part that is rejected in Libre:
https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/rtl_nic

When I eventually compile for a laptop, like the Thinkpad X200, I'll have to
load in the IWLWIFI Intel WiFi driver, but that'll be explicit at least.

## Configuration and build

Things to note are turning off kernel modules (everything Y not M), enabling
livepatching (!), and then selecting KVM, Btrfs, and i915 (+Framebuffer; see
issue #1). There's LUKS decryption built in via BLK_DEV_DM. Make sure the crypto
algorithm being used is included; cryptsetup defaulted to AES XTS for me.

The entire configuration process took 3 hours. Which, really, is less time than
my coworkers spend on ESLint/Webpack/Typescript configs. I deferred to Arch's
kernel config when I didn't understand a section and there weren't recommendend
actions.

Gentoo has a config flag that enables all the needed systemd flags, so refer to
that patch to make sure it'll work.

- https://git.archlinux.org/svntogit/packages.git/tree/trunk/config?h=packages/linux
- https://gitweb.gentoo.org/proj/linux-patches.git/tree/

Run "make -j5". **On a 2010 quad core it takes 330s from scratch. The image is
48M(7.4MB gzip)** which is only ~3MB larger than official Arch/Parabola kernels.

## Artifacts

The image is **arch/x86/boot/bzImage**. x86_64 symlinks to x86. The vmlinux file
created at the root is an ELF executable, which isn't bootable. Use `file` to
check it out. There's also arch/x86/boot/vmlinux.bin and other similarly named
files in arch/x86/boot/compressed/, but they don't seem to be useful to me.

https://en.wikipedia.org/wiki/Vmlinux#bzImage

## Sizing

The kernel leaves "built-in.a" files around the tree. This is unfortunately the
best way I can find for getting a bundle analysis. Too used to Webpack stats
that I assumed there would be pie charts.

Below are some printouts for when I compiled. These built-in.a files stay around
even if you turn off the config for a module (which makes sense, since it's only
focused on being a build cache, not about the final image linking).

For a more accurate reading run "make clean".

```
yesterday@lychee ~/_/w/l/linux-5.6.3> find . -name "built-in.a" | xargs size | sort -n -r -k 4
  25080	    997	 273876	 299953	  493b1	./kernel/printk/printk.o (ex ./kernel/printk/built-in.a)
  25080	    997	 273876	 299953	  493b1	./kernel/printk/printk.o (ex ./kernel/built-in.a)
 157180	  77497	   1032	 235709	  398bd	./fs/ext4/super.o (ex ./fs/ext4/built-in.a)
 157180	  77497	   1032	 235709	  398bd	./fs/ext4/super.o (ex ./fs/built-in.a)
 166488	  54082	    816	 221386	  360ca	./arch/x86/kvm/x86.o (ex ./arch/x86/kvm/built-in.a)
 166488	  54082	    816	 221386	  360ca	./arch/x86/kvm/x86.o (ex ./arch/x86/built-in.a)
 102421	  70140	      0	 172561	  2a211	./fs/btrfs/super.o (ex ./fs/built-in.a)
 102421	  70140	      0	 172561	  2a211	./fs/btrfs/super.o (ex ./fs/btrfs/built-in.a)
 138835	      0	      0	 138835	  21e53	./lib/zstd/compress.o (ex ./lib/zstd/built-in.a)
 138835	      0	      0	 138835	  21e53	./lib/zstd/compress.o (ex ./lib/built-in.a)
  77237	  34767	   8608	 120612	  1d724	./kernel/trace/trace.o (ex ./kernel/trace/built-in.a)
  77237	  34767	   8608	 120612	  1d724	./kernel/trace/trace.o (ex ./kernel/built-in.a)
  96654	  10503	   8468	 115625	  1c3a9	./arch/x86/kvm/vmx/vmx.o (ex ./arch/x86/kvm/built-in.a)
  96654	  10503	   8468	 115625	  1c3a9	./arch/x86/kvm/vmx/vmx.o (ex ./arch/x86/built-in.a)
  91353	  12311	     88	 103752	  19548	./arch/x86/kvm/mmu/mmu.o (ex ./arch/x86/kvm/built-in.a)
  91353	  12311	     88	 103752	  19548	./arch/x86/kvm/mmu/mmu.o (ex ./arch/x86/built-in.a)
  99676	   1457	     56	 101189	  18b45	./fs/btrfs/inode.o (ex ./fs/built-in.a)
  99676	   1457	     56	 101189	  18b45	./fs/btrfs/inode.o (ex ./fs/btrfs/built-in.a)
  93615	   5420	    408	  99443	  18473	./net/core/dev.o (ex ./net/core/built-in.a)
  93615	   5420	    408	  99443	  18473	./net/core/dev.o (ex ./net/built-in.a)
  90054	   4477	   1084	  95615	  1757f	./kernel/events/core.o (ex ./kernel/events/built-in.a)
  90054	   4477	   1084	  95615	  1757f	./kernel/events/core.o (ex ./kernel/built-in.a)
  92727	     97	      0	  92824	  16a98	./arch/x86/kvm/emulate.o (ex ./arch/x86/kvm/built-in.a)
  92727	     97	      0	  92824	  16a98	./arch/x86/kvm/emulate.o (ex ./arch/x86/built-in.a)
  71618   14789      48   86455   151b7 ./drivers/ata/libata-core.o (ex ./drivers/built-in.a)
...
```

Introducing i915 is heavy. The kernel bounces from 48MB(7.4MB gzip) to
53MB(8.2MB gzip)

```
yesterday@lychee ~/_/w/l/linux-5.6.3> find . -name "built-in.a" | xargs size | sort -n -r -k 4 | head -n 20
  25080	    997	 273876	 299953	  493b1	./kernel/printk/printk.o (ex ./kernel/printk/built-in.a)
  25080	    997	 273876	 299953	  493b1	./kernel/printk/printk.o (ex ./kernel/built-in.a)
 157180	  77497	   1032	 235709	  398bd	./fs/ext4/super.o (ex ./fs/ext4/built-in.a)
 157180	  77497	   1032	 235709	  398bd	./fs/ext4/super.o (ex ./fs/built-in.a)
 166488	  54082	    816	 221386	  360ca	./arch/x86/kvm/x86.o (ex ./arch/x86/kvm/built-in.a)
 166488	  54082	    816	 221386	  360ca	./arch/x86/kvm/x86.o (ex ./arch/x86/built-in.a)
 192941	   2062	      0	 195003	  2f9bb	./drivers/gpu/drm/i915/display/intel_display.o (ex ./drivers/gpu/drm/i915/built-in.a)
 192941	   2062	      0	 195003	  2f9bb	./drivers/gpu/drm/i915/display/intel_display.o (ex ./drivers/gpu/drm/built-in.a)
 192941	   2062	      0	 195003	  2f9bb	./drivers/gpu/drm/i915/display/intel_display.o (ex ./drivers/gpu/built-in.a)
 192941	   2062	      0	 195003	  2f9bb	./drivers/gpu/drm/i915/display/intel_display.o (ex ./drivers/built-in.a)
 102421	  70140	      0	 172561	  2a211	./fs/btrfs/super.o (ex ./fs/built-in.a)
 102421	  70140	      0	 172561	  2a211	./fs/btrfs/super.o (ex ./fs/btrfs/built-in.a)
 138835	      0	      0	 138835	  21e53	./lib/zstd/compress.o (ex ./lib/zstd/built-in.a)
 138835       0       0  138835   21e53 ./lib/zstd/compress.o (ex ./lib/built-in.a)
...
```
