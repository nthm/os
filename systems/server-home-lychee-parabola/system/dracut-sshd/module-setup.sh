#!/bin/bash

# 2018, Georg Sauthoff <mail@gms.tf>
# SPDX-License-Identifier: GPL-3.0-or-later

# Edited 2020 for Lychee
# - Force initrd-specific host keys since the partition is unencrypted

authorized_keys=/root/.ssh/authorized_keys

check() {
    require_binaries sshd || return 1

    if [ -z "$(find /etc/ssh -maxdepth 1 -name 'initrd_ssh_host_*_key')" ]; then
        dfatal "sshd: No keys at /etc/sshd/initrd_ssh_host_\*_key. Create some and try again"
        return 1
    fi

    if [ ! -r "$authorized_keys" ]; then
        dfatal "sshd: No authorized_keys for root user found. Symlink it to your primary user"
        return 1
    fi

    # 0 enables by default, 255 only on request
    return 0
}

depends() {
    echo systemd-networkd
}

install() {
    local key_type ssh_host_key
    for key_type in dsa ecdsa ed25519 rsa; do
        ssh_host_key=/etc/ssh/initrd_ssh_host_"$key_type"_key
        if [ -f "$ssh_host_key" ]; then
            inst_simple "$ssh_host_key".pub /etc/ssh/ssh_host_"$key_type"_key.pub
            /usr/bin/install -m 600 "$ssh_host_key" "$initdir/etc/ssh/ssh_host_${key_type}_key"
        fi
    done

    mkdir -p -m 0700 "$initdir/root"
    mkdir -p -m 0700 "$initdir/root/.ssh"
    /usr/bin/install -m 600 "$authorized_keys" "$initdir/root/.ssh/authorized_keys"

    inst_simple /usr/bin/sshd
    inst_simple "${moddir}/sshd.service" "$systemdsystemunitdir/sshd.service"
    inst_simple "${moddir}/sshd_config" /etc/ssh/sshd_config

    # Create privilege seperation directories for sshd
    mkdir -p -m 0755 "$initdir/var/empty"

    systemctl --root "$initdir" enable sshd

    # The systemd-networkd module doesn't include these
    inst_multiple -o /etc/systemd/network/*

    # Start with bash
    echo "root:x:0:0::/root:/bin/bash" >> "$initdir/etc/passwd"

    # Add command to bash history for easier access
    echo systemd-tty-ask-password-agent >> "$initdir/root/.bash_history"
    chmod 600 "$initdir/root/.bash_history"
    cat << 'EOT' > "$initdir/root/.bashrc"
export PS1="\l \w \t >_ \[$(tput sgr0)\]"
export TERM=linux
EOT
    return 0
}
