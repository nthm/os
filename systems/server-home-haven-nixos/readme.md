# NixOS server

_Update:_ I completely replaced NixOS with a Parabola install for servers. It
wasn't worth the issues.

See _server-home-lychee/_

---

Less than ideal. I read the thesis and agree with the theory, but in practice
NixOS and Nix have a great deal of NIH and have clearly bitten off more than
they can handle. The 2500+ issues on GitHub speaks well to that.

I ended up running Arch containers ontop of the NixOS base to dodge many of its
shortcomings.

Treating NixOS as an autoupdating _foundation_ with very few programs. It's a
hassle to try and iteratively development/setup in a Nix environment. Takes a
lot more time since the OS is mounted RO and basic folders/concepts don't exist:

```
lavender@haven ~> sudo systemctl enable systemd-nspawn@syncthing.service
Failed to enable unit: Unit file /etc/systemd/system/machines.target.wants/systemd-nspawn@syncthing.service does not exist.
lavender@haven ~> ll /etc/systemd/system/machines.target.wants/
ls: cannot access '/etc/systemd/system/machines.target.wants/': No such file or directory
```

This article summarizes NixOS well: https://push.cx/2018/nixos

> If I sound bewildered and frustrated, then I’ve accurately conveyed the
> experience.

Much of the configuration.nix doesn't work, or is unpredicatable depending on
the system state (ironic, I know). There are some comments and workarounds, but
I wouldn't recommend it.
