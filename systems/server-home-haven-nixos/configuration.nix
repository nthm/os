{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot/efi";
      grub.efiSupport = true;
      grub.enableCryptodisk = true;
    };
    initrd.luks.devices = {
      os = {
        device = "/dev/disk/by-uuid/f11bae03-3026-4ab4-a081-db293950ea2f";
        allowDiscards = true;
        keyFileSize = 4096;
        keyFile = "/dev/disk/by-uuid/FD39-D0A0";
      };
      mirrorA = {
        device = "/dev/disk/by-uuid/92de733c-7701-4d7a-bfd4-b0cfb3cfd5d3";
        keyFileSize = 4096;
        keyFile = "/dev/disk/by-uuid/FD39-D0A0";
      };
      mirrorB = {
        device = "/dev/disk/by-uuid/b88a16ec-a89b-4e0b-8783-f2840b04149f";
        keyFileSize = 4096;
        keyFile = "/dev/disk/by-uuid/FD39-D0A0";
      };
    };

    # Mount USB key before trying to decrypt root filesystem
    initrd.postDeviceCommands = pkgs.lib.mkBefore "sleep 10";

    # initrd.kernelModules = [ "r8169" ];
    # initrd.network = {
    #   enable = false;
    #   ssh = {
    #     enable = false;
    #     port = 22;
    #     shell = "/bin/cryptsetup-askpass";
    #     hostRSAKey = "/run/keys/initrd-ssh-key"; 
    #     authorizedKeys = [ "" ];
    #   };
    # };
  };

  fileSystems = {
    "/boot/efi" = {
      device = "UUID=FD39-D0A0";
      fsType = "vfat";
    };
    "/data" = {
      device = "/dev/mapper/mirrorA"; # Mounting mirrorA also pulls in mirrorB
      fsType = "btrfs";
      options = [ "nofail" ];
    };
  };

  i18n = {
    consoleFont = "sun12x22";
    consoleKeyMap = "us";
    defaultLocale = "en_CA.UTF-8";
  };

  time.timeZone = "America/Vancouver";

  powerManagement.powerUpCommands = ''
    ${pkgs.hdparm}/sbin/hdparm -B 255 /dev/disk/by-id/ata-WDC_WD20EFRX-68EUZN0_WD-WCC4M1CAHRP4
    ${pkgs.hdparm}/sbin/hdparm -B 255 /dev/disk/by-id/ata-ST1000DM003-1CH162_S1D8SGCW
  '';

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  # environment.systemPackages = with pkgs; [
  #   wget vim
  # ];

  programs.fish.enable = true;

  services.openssh = {
    enable = true;
    authorizedKeysFiles = ["/home/lavender/.ssh/authorized_keys"];
    passwordAuthentication = true;
    permitRootLogin = "yes";
  };

  # Prevent brute force attacks. Errors out with "226/NAMESPACE"
  # services.sshguard = {
  #   enable = true;
  # };

  services.resolved = {
    enable = true;
    dnssec = "false"; # Yes it's a string. Off for now.
  };

  services.caddy = {
    enable = true;
    config = ''
      haven.local:80

      tls off

      # basicauth /sync lavender laven
      proxy /sync localhost:8384 {
        transparent
      }

      timeouts {
        read none
        write none
        header none
      }
    '';
  };

  # Difficult to configure. Ran in a container instead.
  # services.syncthing = {
  #   enable = true;
  # };

  systemd.services."local-nspawn@" = {
    description = "Container %i";
    documentation = [ "man:systemd-nspawn(1)" ];
    partOf = [ "machines.target" ];
    before = [ "machines.target" ];
    after = [ "network.target" "systemd-resolved.service" ];

    # NisOS doesn't support this option...
    # RequiresMountsFor=/var/lib/machines

    serviceConfig = {
      # Removed --network-veth since NixOS does not support systemd-networkd very well
      ExecStart = "${pkgs.systemd}/bin/systemd-nspawn --quiet --keep-unit --boot --link-journal=try-guest -U --settings=override --machine=%i";
      KillMode = "mixed";
      Type = "notify";
      RestartForceExitStatus = "133";
      SuccessExitStatus = "133";
      WatchdogSec = "3min";
      Slice = "machine.slice";
      Delegate = "yes";
      TasksMax = "1000";
    };
    wantedBy = [ "multiuser.target" ];
  };

  # This doesn't work. It creates a new service rather than use the template
  # systemd.services."local-nspawn@syncthing".enable = true;

  # sudo systemctl enable local-nspawn@syncthing.service
  # Failed to enable unit: File /etc/systemd/system/local-nspawn@syncthing.service: Read-only file system

  systemd.nspawn.syncthing = {
    enable = true;
    networkConfig = {
      # This seems to be nothing
      Private = false;
    };
    wantedBy = [ "multi-user.target" ];
  };

  # Doesn't seem to work either
  systemd.network.enable = true;
  systemd.network.links.enp4s0.enable = false;
  systemd.network.links.wlp3s0.enable = false;

  # systemd.network.networks.ve = {
  #   matchConfig = {
  #     Name = "ve-*";
  #     Driver = "veth";
  #   };
  #   networkConfig = {
  #     Address = "0.0.0.0/28";
  #     LinkLocalAddressing = "yes";
  #     DHCPServer = "yes";
  #     IPMasquerade = "yes";
  #     LLDP = "yes";
  #     EmitLLDP = "customer-bridge";
  #   };
  # };

  # trace: Systemd Network has extra fields [Address EmitLLDP LLDP LinkLocalAddressing].
  # error: The option value `systemd.network.networks.ve.networkConfig' in `/etc/nixos/configuration.nix' is not of type `attribute set of systemd options'.

  systemd.services.systemd-networkd-wait-online.serviceConfig.ExecStart = [
    "${config.systemd.package}/lib/systemd/systemd-networkd-wait-online --ignore enp4s0 --ignore wlp3s0"
  ];

  # LUKS. Because NixOS isn't able to use the USB key on boot...somehow
  systemd.generator-packages = [
    pkgs.systemd-cryptsetup-generator
  ];

  environment.etc = {
    "crypttab" = {
      enable = true;
      text = ''
        mirrorA UUID=92de733c-7701-4d7a-bfd4-b0cfb3cfd5d3 /dev/disk/by-uuid/FD39-D0A0 luks,keyfile-size=4096
        mirrorB UUID=b88a16ec-a89b-4e0b-8783-f2840b04149f /dev/disk/by-uuid/FD39-D0A0 luks,keyfile-size=4096
      '';
    };
  };

  networking = {
    hostName = "haven"; 

    # NixOS is not ready as a community to support this.
    useNetworkd = false;

    useDHCP = true;
    dnsExtensionMechanism = true;
    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
      "2606:4700:4700::1111"
      "2606:4700:4700::1001"
    ];
    firewall = {
      allowedTCPPorts = [ 22 80 443 ];
      allowedUDPPorts = [];
      allowPing = true;
    };
  };

  # Allow passwords to be set imperatively
  users.mutableUsers = true;

  users.extraUsers.lavender = {
    isNormalUser = true;
    shell = "/run/current-system/sw/bin/fish";
    extraGroups = [ "wheel" ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  # system.stateVersion = "18.09";
  system.autoUpgrade.enable = true;
}
