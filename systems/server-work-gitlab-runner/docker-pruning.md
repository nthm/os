## Maintenance and Pruning

GitLab runners that run _a lot_ of jobs as a Docker executor can end up with
hundreds of unused containers and images. This is usually because devs pick the
":latest" tag so a new images is pulled automatically too often. To prevent the
hosts from running out of space use pruning:

https://docs.docker.com/config/pruning/

_/etc/systemd/system/docker-prune.service_:
```
[Unit]
Description=Docker prune containers, images, and volumes

[Service]
Type=oneshot
ExecStart=/usr/bin/docker system prune --force --volumes

[Install]
WantedBy=multi-user.target
```

_/etc/systemd/system/docker-prune.timer_:
```
[Unit]
Description=Weekly prune of Docker data

[Timer]
# https://wiki.archlinux.org/index.php/Systemd/Timers#Realtime_timer
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
```
