Documenting a server that ran GitLab CI builds and hosted deployments from the
build artifiacts. It was a Docker executor for builds, and was a shell executor
to run deployments via Docker Compose, updating a Portainer Stack. Latests build
images were pushed and pulled from our GitLab container registry.

I'd much rather use _systemd-nspawn_ for container deployments but the team
needed something well supported and "enterprise ready", so Docker+Portainer is
the best option.
