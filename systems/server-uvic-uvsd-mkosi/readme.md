# GitLab + Shibboleth SSO

These files configure mkosi to build a CentOS 7 image setup with GitLab Omnibus,
Shibboleth SSO, and Apache all tied together.

Note: May need to add the uvsd.ca IP to the whitelisted IPs section of
shibboleth2.xml. This was needed when shibd was running in a Docker container
under another IP address and would normally reject non-localhost connections.

## How to

You'll need mkosi to build this image. It's part of the systemd project, and is
a wrapper around existing OS bootstrap scripts such as pacstrap or debootstrap.

Download this directory tree. Consider creating a _mkosi.cache/_ folder to cache
package downloads (optional).

Run `sudo mkosi` to build. This takes some time.

Run `sudo systemd-nspawn -D uvsdgitlab` to chroot into the OS tree. From here
you can run passwd to set the root password. Exit.

Run `sudo systemd-nspawn -bD uvsdgitlab` to boot the container.

Note that the mkosi.default has a `Format=` field which specifies the build
image format. The 'directory' type is most simple for inspecting the result
since you don't need to bind loop mounts or use nspawn to see the files.

The CentOS base, Apache, and Shibboleth takes around 350MB. Adding GitLab brings
the image size to 1.4GB (!)

## TODO

- Configure GitLab
- Setup SMTP settings for UVic email so GitLab can email people
- Bind mount directories for GitLab data and backups to be stored outside of the
  container

```ruby
external_url 'https://git.uvsd.ca'
gitlab_rails['internal_api_url'] = 'https://git.uvsd.ca'

# Disable Nginx
nginx['enable'] = false

gitlab_rails['omniauth_allow_single_sign_on'] = true
gitlab_rails['omniauth_block_auto_created_users'] = false
gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_providers'] = [
  {
    "name"  => "shibboleth",
    "label" => "Text for Login Button",
    "args"  => {
        "shib_session_id_field"     => "HTTP_SHIB_SESSION_ID",
        "shib_application_id_field" => "HTTP_SHIB_APPLICATION_ID",
        "uid_field"                 => 'HTTP_EPPN',
        "name_field"                => 'HTTP_CN',
        "info_fields" => { "email" => 'HTTP_MAIL'}
    }
  }
]
```

Haven't decided if I'll try supporting sign in with Slack (ugh) but if so then
the main Gemfile (at /usr/share/webapps/gitlab/Gemfile on a source install on
Arch) needs to have:

```
# Authentication libraries
gem 'devise', '~> 4.4'
gem 'doorkeeper', '~> 4.3'
gem 'doorkeeper-openid_connect', '~> 1.5'
gem 'omniauth', '~> 1.8'
gem 'omniauth-cas3', '~> 1.1.4'
gem 'omniauth-shibboleth', '~> 1.3.0'
gem 'rack-oauth2', '~> 1.2.1'
gem 'jwt', '~> 1.5.6'
# gem 'ginjo-omniauth-slack'
gem 'omniauth-slack', :git => 'https://github.com/ginjo/omniauth-slack.git', :branch => 'features-and-fixes'
```

Then run:

```
cd /usr/share/webapps/gitlab
sudo env EXECJS_RUNTIME=Disabled RAILS_ENV=production bundle-2.5 install --no-deployment --without development test mysql aws kerberos --path vendor/bundle
```

To install the new gem and remove the other conflicting OAuth ones. Note that
since that removes sign in with Google, Twitter, etc then enabling those will
cause GitLab to fail to start.

It's tempting to use just use `gem 'ginjo-omniauth-slack'` like the repo says,
but that doesn't work.

## Hand configuration for GitLab

- Admin Area > Appearance > Sign in/Sign up pages > Title

- Admin Area > Appearance > Sign in/Sign up pages > Logo

- Admin Area > Settings > CI/CD > (uncheck) Default to Auto DevOps pipeline for
  all projects

- Admin Area > Settings > CI/CD > (uncheck) Enable shared runners for new
  projects

- Admin Area > Settings > Reporting > Abuse Reports > Abuse reports notification
  email > ...@uvic.ca

- Admin Area > Settings > Network > User and IP Rate Limits > (check) Enable
  unauthenticated request rate limit

- Admin Area > Settings > General > Account and limit > Default projects limit >
  50

- Admin Area > Settings > General > Account and limit > Maximum attachment size
  (MB) > 10

- Admin Area > Settings > General > Sign-up restrictions > (uncheck) Sign-up
  enabled

- Admin Area > Settings > General > Account and limit > (uncheck) Gravatar
  enabled

- Admin Area > Settings > Integrations > Third party offers > (check) Do not
  display offers from third parties within GitLab

- Admin Area > Settings > General > Terms of Service and Privacy Policy
