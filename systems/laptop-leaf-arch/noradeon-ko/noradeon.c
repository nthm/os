/*
	Turns off the ATI Radeon graphics card by calling its _OFF method (defined
    in its SSDT) and then replaces the _INI method to redirect to _OFF in case
    the card is started again.
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/acpi.h>

MODULE_AUTHOR("Grant Hames-Morgan <nthmorgan@gmail.com>");
MODULE_DESCRIPTION("Disables the discrete ATI Radeon graphics card in an HP Envy 1154ca");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1");

static u8 _INI_Method_Aml[] = {
	0x53, 0x53, 0x44, 0x54, 0x55, 0x00, 0x00, 0x00,  /* 00000000    "SSDTU..." */
	0x01, 0xB7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  /* 00000008    "........" */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  /* 00000010    "........" */
	0x00, 0x10, 0x00, 0x00, 0x49, 0x4E, 0x54, 0x4C,  /* 00000018    "....INTL" */
	0x17, 0x07, 0x15, 0x20, 0x14, 0x30, 0x5C, 0x2F,  /* 00000020    "... .0\/" */
	0x05, 0x5F, 0x53, 0x42, 0x5F, 0x50, 0x43, 0x49,  /* 00000028    "._SB_PCI" */
	0x30, 0x50, 0x30, 0x50, 0x32, 0x50, 0x45, 0x47,  /* 00000030    "0P0P2PEG" */
	0x50, 0x5F, 0x4F, 0x4E, 0x5F, 0x08, 0x5C, 0x2F,  /* 00000038    "P_ON_.\/" */
	0x05, 0x5F, 0x53, 0x42, 0x5F, 0x50, 0x43, 0x49,  /* 00000040    "._SB_PCI" */
	0x30, 0x50, 0x30, 0x50, 0x32, 0x50, 0x45, 0x47,  /* 00000048    "0P0P2PEG" */
	0x50, 0x5F, 0x4F, 0x46, 0x46                     /* 00000050    "P_OFF"    */
};

static int __init noradeon_init(void) {

	char * method = "\\\\_SB.PCI0.P0P2.PEGP._OFF";
	acpi_status status;
    acpi_handle handle;

    // Replace the ACPI method
	status = acpi_install_method(_INI_Method_Aml);
	if (ACPI_FAILURE(status)) {
		pr_err("Failed to replace _INI method in the SSDT\n");
		return -1;
	}

    status = acpi_get_handle(NULL, (acpi_string) method, &handle);
    if (ACPI_FAILURE(status)) {
        pr_err("Failed to get handle.\n");
		return -1;
	}

    // Turn off the card
    status = acpi_evaluate_object(handle, NULL, NULL, NULL);
    if (ACPI_FAILURE(status)) {
		pr_err("Failed to call _OFF for PEGP card\n");
		return -1;
	}

	pr_info("Radeon patch applied successfully\n");
	return 0;
}

static void __exit noradeon_exit(void) {
	pr_info("Radeon patch unloaded\n");
}

module_init(noradeon_init);
module_exit(noradeon_exit);
