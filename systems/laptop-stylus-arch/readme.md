Stylus - Lenovo ThinkPad X61T

Daily notebook starting early 2019. Made around 2008. Wanted to run Parabola but
the BIOS has a WiFi card whitelist and needs to be flashed; currently on Arch.
No hardware issues, not even for the stylus. See _config/_

Running sway, swaylock, waybar, thunar, xournalpp

![](./photo.png)
