# uvsd.ca

Mirror of the backup files for the uvsd.ca server. Outlines the configuration as
well.

UVic allows ports 22, 80, 443, 8080, 8443

Used `/srv` for GitLab, SVN, and containers. It's large.

## Services

### NGINX

Lead server which manages ports 80 and 443 (all HTTP is redirected to HTTPS)

### Shibboleth + Apache

For university SAML login into the GitLab (and therefore also Mattermost). Used
to be a CentOS 7 container but recently switched to an Alpine Docker image.

Shibboleth is its own daemon, but all GitLab traffic is sent to the container's
Apache daemon which uses a `mod_proxy` to authenticate Shibboleth sessions. Not
awesome but it works; there's no stable native NGINX alternative for proxying

### GitLab

NGINX used to redirect git.uvsd.ca packets to GitLab's workhorse, but now passes
through the Shib + Apache container first to support Shibboleth sessions. The
redirected traffic is sent from the container to workhorse and then back again

### SVN + Apache

NGINX does not have DAV support to talk to the SVN over HTTP and passing
svn.uvsd.ca packets to Apache is the best option, unfortunately

### SSH

Secured `sshd` with no root remote login, short session timeouts, and two factor
authetication (TOTP) for connections that don't use a public key

### Other

Setup Fail2ban, btrbk, Mattermost, and PostgreSQL as well

## Setup

Standard minimal Arch Linux install, up to the bootloader

GRUB UEFI install:

```
grub-mkconfig -o /boot/grub/grub.cfg 
grub-install /dev/sda --target=x86_64-efi --efi-directory=/boot --recheck
```

Needed to play with mdadm to pull SVN data from the older CentOS drive:

```
mdadm --run /dev/md12{4,5,6,7}
mdadm --assemble --force --run /dev/md127
mount /dev/md/UVSD-01:srv /srv
svnserve --daemon --pid-file=/run/svnserve/svnserve.pid -r /srv/svn
```

Stick to the wiki for the GitLab:

```
-rw-r--r-- 1 root   root   7.4K Oct  9 15:57 application.rb
-rw------- 1 gitlab gitlab  908 Oct  2 14:15 database.yml
-rw-rw---- 1 root   gitlab   44 Oct  9 15:57 gitlab_workhorse_secret
-rw-r--r-- 1 root   root    29K Sep 30 01:10 gitlab.yml
-rw-r--r-- 1 root   root   1.1K Sep 11 14:27 resque.yml
-rw-r----- 1 root   gitlab  128 Sep 11 15:34 secret
-rw------- 1 gitlab gitlab 4.5K Sep 29 14:18 secrets.yml
-rw-r--r-- 1 gitlab gitlab 5.5K Oct 19 15:39 unicorn.rb
```

Needed to do a lot of debugging between NGINX, GitLab workhorse/unicorn, Apache,
the networking between Docker and the host...

```
gitlab-workhorse
  -listenUmask 0
  -authBackend http://localhost:8080
  -authSocket /var/lib/gitlab/sockets/gitlab.socket
  -listenAddr /run/gitlab/gitlab-workhorse.socket
  -listenNetwork unix /srv/gitlab/repositories
  -documentRoot /usr/share/webapps/gitlab/public
```

Disable Btrfs COW for PostgreSQL

```
chattr +C /var/lib/postgres/data
lsattr /var/lib/postgres/data/
su - postgres -c "initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'"
```

Mattermost

```
systemctl reload postgresql
systemctl restart postgresql
vim /var/lib/postgres/data/pg_hba.conf
vim /var/lib/postgres/data/pg_ident.conf
sudo -u mattermost psql --dbname=mattermostdb --username=mattermost
```

Setup archive.uvsd.ca to use h5ai to browse files

```
curl -O https://release.larsjung.de/h5ai/h5ai-0.29.0.zip
yaourt -S php-fpm
vim /etc/php/php-fpm.conf 
vim /etc/php/php-fpm.d/www.conf 
systemctl enable php-fpm --now
vim /etc/nginx/fastcgi.conf 
```

LetsEncrpt needs to access .well-known without being redirected by NGINX to
an HTTPS site, so conf.d/letsencrypt.conf allows that (see repo) then just
follow the Certbot Arch wiki

---

_No longer in use_: systemd-nspawn webserver process isolation

```
mkdir -p /srv/containers/
yaourt -S arch-install-scripts
pacstrap -i -c -d /srv/containers/sync base --ignore linux
mkdir -p /var/lib/containers/sync
ln -s /srv/containers/sync /var/lib/containers/sync
mkdir /etc/systemd/system/systemd-nspawn@sync.service.d 
systemctl reload systemd-nspawn@sync.service
systemctl enable systemd-nspawn@sync.service --now
systemctl enable systemd-networkd.service --now
machinectl login sync # to test the container
pacman --root /var/lib/containers/sync/ -S ping iproute2 ...
```

## Explicitly installed packages + AUR

This list is out of date but provides an idea...

```
apache
certbot-nginx
dhcpcd
efibootmgr
fail2ban
fakeroot
fish
gitlab
go
gptfdisk
grub
htop
iproute2
iputils
jfsutils
logrotate
lsof
lvm2
mdadm
memcached
nginx
npm
oath-toolkit
pass
php-fpm
pkgfile
polkit
postgresql
psmisc
qrencode
rsync
ruby
s-nail
strace
subversion
sudo
```
