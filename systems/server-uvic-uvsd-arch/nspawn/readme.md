This isn't being used anymore. For a long time two nspawn containers supported
an environment for a package to run, but now Docker is being used instead

I don't like Docker, and would rather keep it closer to suckless by just using
nspawn. Unfortunately it just makes more sense to have a Docker Alpine image
that's reproducible with a Dockerfile

Also, Cockpit has support for Docker images, which could be nice.
