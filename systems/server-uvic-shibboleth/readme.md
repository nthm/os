_Archived_: This isn't used since the GitLab and Shibboleth services were sent
away into a CentOS container. See the `shib/` sibling folder for the files.

Based off the _apache-shib-sp-base_ setup on https://github.com/ojbc/docker/,
commit 59ed6ea75127f30363a3f9a355377e97090d208f. It's RPL license, so I guess
I'll play that game here...

This part of the repo is a "fork", which can't really be done since it's from
GitHub and this isn't on that domain

The goal for the container is to host not only the Shibboleth daemon but also a
webserver. Luckily this image does just that, on top of appearing nice and light

Here are the files needed by the container:

  - _attribute-map.xml_
  - _demostate-ctf.xml_
  - _shib.conf_ (which is their name for the httpd.conf)
  - _metadata.crt_
  - _shibboleth2.xml_
  - _sp-cert.pem_
  - _sp-key.pem_

Private files, PEMs and CRTs, are not uploaded to Git

Needed to update all the versions of the packages

The final build is 650MB. 200-300MB of that is Boost C++ libraries. ~100MB is
used by the Xerces-C dependency. It's hard to say what's a build-time
dependency because I'm not a C++ developer. Not sure if it can be minified.

---

__UPDATE__: Interestingly enough, I was pressed for time so I downloaded a
CentOS image that had Shibboleth pre-installed. I thought it would be 1GB at
least, considering the nspawn raw image for CentOS I had previously was 4GB. It
wasn't; it was 340MB. That's half the size of the Alpine image and far less
configuration. So I scrapped the Alpine since it's setup and running now.

__UPDATE__: No longer supporting Docker on the server. Moved GitLab and
Shibboleth to a CentOS nspawn container.
